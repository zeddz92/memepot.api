<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Users
 */
Route::resource('users', 'User\UserController', ['except' => ['create', 'edit']]);
Route::resource('users.followers', 'User\UserFollowerController', ['only' => ['index']]);
Route::resource('users.following', 'User\UserFollowingController', ['only' => ['index', 'store', 'delete']]);
Route::resource('users.posts', 'User\UserPostController', ['only' => ['index']]);
Route::resource('users.feed', 'User\UserFeedController', ['only' => ['index']]);
Route::resource('users.settings', 'User\UserSettingController', ['only' => ['update', 'edit']]);
Route::put('users/{user}/settings', 'User\UserController@updateSetting');
Route::get('users/{id}/summary', 'User\UserController@summary');

Route::resource('users/following/{user}', 'User\UserFollowingController', ['only' => ['index', 'store']]);
Route::delete('users/following/{user}', 'User\UserFollowingController@destroy');

Route::resource('users.favorites', 'User\UserFavoriteController', ['only' => ['store']]);
Route::delete('users/{user}/favorites', 'User\UserFavoriteController@destroy');
/**
 * Tags
 */
Route::resource('tags', 'Tag\TagController', ['only' => ['index']]);

/**
 * Posts
 */
Route::resource('posts', 'Post\PostController', ['except' => ['create', 'edit']]);
Route::resource('posts.comments', 'Post\PostCommentController', ['only' => ['index', 'update', 'store', 'destroy']]);
Route::resource('posts/{post}/feedbacks', 'Post\PostFeedBackController', ['except' => ['create', 'edit']]);
Route::resource('posts.strikes', 'Post\PostStrikeController', ['only' => ['store']]);

/**
 * Comments
 */
Route::resource('comments', 'Comment\CommentController', ['only' => ['show']]);
Route::resource('comments.replies', 'Comment\CommentReplyController', ['only' => ['index']]);
Route::resource('comments.feedbacks', 'Comment\CommentFeedBackController', ['only' => ['store']]);

//Route::post('comments/{id}/feedbacks', 'Comment\CommentController@feedback');
Route::resource('comments.strikes', 'Comment\CommentStrikeController', ['only' => ['store']]);


/*<<<<<<< Updated upstream
Route::put('comments/{id}', 'Comment\CommentController@update');
Route::delete('comments/{id}', 'Comment\CommentController@destroy');
Route::post('comments/{id}', 'Comment\CommentController@comment'); // create comment or reply
Route::post('comments/{id}/feedbacks', 'Comment\CommentController@feedback');
Route::delete('comments/{id}', 'Comment\CommentController@destroy');
Route::put('comments/{id}', 'Comment\CommentController@update');
Route::post('comments/{id}', 'Comment\CommentController@comment'); // create comment or reply
=======
Route::resource('comments/{comment}/feedbacks', 'Comment\CommentFeedBackController', ['except' => ['create', 'edit']]);
>>>>>>> Stashed changes*/




Route::name('verify')->get('users/verify/{token}', 'User\UserController@verify');
Route::name('resend')->get('users/{user}/resend', 'User\UserController@resend');

/**
 * Auth
 */

Route::name('forgot_password')->post('forgot_password', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::post('users/{user}/change_password', 'Auth\ChangePasswordController@changePassword');
Route::post('login', 'Auth\LoginController@login');
Route::post('login_social', 'Auth\LoginController@loginWithSocialMedia');



Route::post('register', 'Auth\RegisterController@register');

//Route::name('forgot_password')->post('forgot_password', 'Auth\ForgotPasswordController');

/**
 * Posts filters
 */
Route::get('posts/filters/{filter}', 'Post\PostController@filter');



Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


// Route::resource('posts/feedbacks', 'Post\PostFeedBackController', ['except' => ['create', 'edit']]);
