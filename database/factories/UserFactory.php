<?php

use \App\Post;
use \App\User;
use \App\Follow;
use \App\Media;
use \App\Tag;
use \App\Comment;
use \App\PostFeedBack;
use \App\CommentFeedBack;
use \App\Favorite;
use Faker\Generator as Faker;


/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

function createThumbnail($name, $img, $height, $width)
{
//    $imagick = new Imagick(realpath($img));
//    $imagick->setImageFormat('jpeg');
//    $imagick->setImageCompression(Imagick::COMPRESSION_JPEG);
//    $imagick->setImageCompressionQuality(10);
//    $imagick->thumbnailImage($width, $height, false, false);
//    $filename_no_ext = explode('.', $name)[0];
//    if (file_put_contents('../public/thumb/' . $filename_no_ext . '_thumb' . '.jpg', $imagick) === false) {
//        throw new Exception("Could not put contents.");
//    }
}


$factory->define(User::class, function (Faker $faker) {

    return [
        'name' => $faker->name,
        'username' => $faker->unique()->userName,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$LVlZ.25YPEP8a72.qQ4Sh.8oxxns29LUhQnOeqOIfqEOOJ2PsFUpC', // secret
        'profile_picture' => $faker->image('public/assets/profile_pictures', 400, 300, null, false),
        'status' => 1,
        'remember_token' => str_random(10),
        'verified' => $verified = $faker->randomElement([User::VERIFIED_USER, User::UNVERIFIED_USER]),
        'verification_token' => $verified == User::VERIFIED_USER ? null : User::generateVerificationCode(),
        'admin' => $admin = $faker->randomElement([User::ADMIN_USER, User::REGULAR_USER]),
    ];
});

$factory->define(Comment::class, function (Faker $faker) {

    $user = User::all()->random();

    return [
        'parent_id' => null,
        'user_id' => $user->id,
        'text' => $faker->sentence(6),
    ];
});

$factory->define(Favorite::class, function (Faker $faker) {

    $user = User::all()->random();
    $post = Post::all()->random();

    return [
        'user_id' => $user->id,
        'post_id' => $post->id,
    ];
});


$factory->define(Follow::class, function (Faker $faker) {

    $user = User::all()->random();
    $follower = User::all()->except($user->id)->random();

    return [
        'user_id' => $user->id,
        'follower_id' => $follower->id,
    ];
});

$factory->define(Post::class, function (Faker $faker) {

    $user = User::all()->random();

    return [
        'title' => $faker->sentence(5),
        'status' => $faker->numberBetween(0, 3),
        'user_id' => $user->id,
    ];
});

$factory->define(PostFeedBack::class, function (Faker $faker) {

//    $postFeedBack = PostFeedBack::all()->pluck('post_id');
    $user = User::all()->random();
    $post = $faker->numberBetween(1, 550);

    return [
        'post_id' => $post,
        'user_id' => $user->id,
        'feedback' => $faker->numberBetween(1, 2),
    ];
});

$factory->define(CommentFeedBack::class, function (Faker $faker) {

//    $commentFeedBack = CommentFeedBack::all()->pluck('user_id');
    $user = User::all()->random();
    $comment = Comment::all()->random();

    return [
        'comment_id' => $comment->id,
        'user_id' => $user->id,
        'feedback' => $faker->numberBetween(1, 2),
    ];
});

$factory->define(Tag::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'description' => $faker->sentence(5),
    ];
});

$factory->define(Media::class, function (Faker $faker) {
    $files = scandir("public/img");

    unset($files[0]);
    unset($files[1]);
//    unset($files[2]);


//    print_r($files);
//    return;

    $randomFile = $faker->numberBetween(3, count($files));

    $file = $files[$randomFile];


    $fileUrl = 'public/img/' . $file;
    $data = getimagesize($fileUrl);
    $type = 1;

    $width = $data[0];
    $height = $data[1];
    $mime = $data['mime'];



    if ((strpos($mime, 'image') !== false)) {
        if (strpos($mime, 'gif')) {
            $type = 2;
        }
    } else if (strpos($mime, 'video') !== false) {
        $type = 3;
    }

    createThumbnail($files[$randomFile], $fileUrl, $height, $width);
    $filename_no_ext = explode('.', $file)[0];


//    unlink($fileUrl);


    return [
        'media_url' => $file,
        'name' => $file,
        'media_type_id' => $type,
        'mime' => $mime,
        'thumbnail_url' => "{$filename_no_ext}_thumb.jpg",
        'width' => $width,
        'height' => $height,

    ];
});




/*$factory->define(Post::class, function (Faker $faker) {
    return [
        'title' => $faker->paragraph(1),
        'user_id' => User::all()->random()->id,
    ];
});


*/



