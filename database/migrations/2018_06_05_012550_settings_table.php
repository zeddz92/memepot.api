<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unique()->unsigned();

            $table->boolean('notification_post_feedback')->default(1);
            $table->boolean('notification_post_reply')->default(1);

            $table->boolean('notification_comment_feedback')->default(1);
            $table->boolean('notification_comment_reply')->default(1);

            $table->boolean('notification_follow')->default(1);

            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
