<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStrikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('strikes', function (Blueprint $table) {
            $table->increments('id');
            $table->string("subject")->nullable();
            $table->integer('user_id')->unsigned();
            $table->integer('strike_user_id')->unsigned();
            $table->integer('reason_id')->unsigned();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('strike_user_id')->references('id')->on('users');
            $table->foreign('reason_id')->references('id')->on('reasons');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('strikes');
    }
}
