<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CommentMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comment_media', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("comment_id")->unsigned();
            $table->integer("media_id")->unsigned();

            $table->foreign('comment_id')->references('id')->on('comments');
            $table->foreign('media_id')->references('id')->on('medias');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comment_media');
    }
}
