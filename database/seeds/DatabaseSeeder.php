<?php

use Illuminate\Database\Seeder;
use \App\Media;
use \App\User;
use \App\Post;
use \App\Follow;
use \App\Tag;
use \App\Comment;
use \App\PostFeedBack;
use \App\CommentFeedBack;
use \App\Favorite;

use \Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
//        User::truncate();
//        Media::truncate();
//        Post::truncate();
//        Follow::truncate();
//        Tag::truncate();
//        Comment::truncate();
//        PostFeedBack::truncate();
//        CommentFeedBack::truncate();


        $usersQuantity = 200;
        $followsQuantity = 2000;
        $postQuantity = 550;
        $mediaQuantity = 550;
        $tagQuantity = 1000;
        $commentQuantity = 5000;
        $postFeedbackQuantity = 3000;
        $commentFeedbackQuantity = 3000;

        $favoriteQuantity = 3000;
        factory(Favorite::class, $favoriteQuantity)->create();


//        factory(User::class, $usersQuantity)->create();
//        factory(Follow::class, $followsQuantity)->create();
//        factory(Tag::class, $tagQuantity)->create();
//        factory(Comment::class, $commentQuantity)->create();
//        factory(PostFeedBack::class, $postFeedbackQuantity)->create();
//        factory(CommentFeedBack::class, $commentFeedbackQuantity)->create();
////        factory(Post::class, $postQuantity)->create();
//        factory(Media::class, $mediaQuantity)->create();
//
//        factory(Comment::class, $commentQuantity)->create()->each(function ($comment){
//            $medias = Media::all()->random(mt_rand(1,3))->pluck('id');
//            $comment->medias()->attach($medias);
//
//
//        });
//
//        factory(Post::class, $postQuantity)->create()->each(function ($post){
//            $medias = Media::all()->random(mt_rand(1,3))->pluck('id');
//            $tags = Tag::all()->random(mt_rand(1,3))->pluck('id');
//            $comments = Comment::all()->random(mt_rand(1,20))->pluck('id');
//
////            $feedback = PostFeedBack::where("post_id", $post->id)->random(mt_rand(1,1000))->pluck('id');
//
////            $post->feedback()->attach($feedback);
//            $post->comments()->attach($comments);
//            $post->medias()->attach($medias);
//            $post->tags()->attach($tags);
//
//        });
//        factory(Media::class, $mediaQuantity)->create()->each(function ($media){
//            $post = Post::all()->random();
//            $media->categories()->attach($post);
//        });

    }
}
