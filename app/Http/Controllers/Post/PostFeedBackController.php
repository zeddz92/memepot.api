<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\ApiController;
use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PostFeedback;
use Illuminate\Validation\Rule;

class PostFeedBackController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Post $post)
    {
        $this->validate($request, [
            'user_id' => ['required', 'exists:users,id'],
            'feedback' => ['required', Rule::in(['1', '2', '0'])]// 1 -> like, 2 -> dislike
        ]);

        $input = $request->all();

        $feedback = PostFeedback::where('user_id', $input['user_id'])
            ->where('post_id', $post->id)->first();

        if (!$feedback) {
            $input["post_id"] = $post->id;
            $feedback = PostFeedback::create($input);

        } else {
            $feedback->update(['feedback' => $input['feedback']]);
        }

        $post = $post->where("id", $post->id)->with(['medias', 'tags', 'user', 'userFeedback'])->withCount(['comments as comments',
            'feedback as likes' => function ($query) {
                $query->where('feedback', 1);
            },
            'feedback as dislikes' => function ($query) {
                $query->where('feedback', 2);
            }
        ])->first();

        return $this->showOne($post);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
