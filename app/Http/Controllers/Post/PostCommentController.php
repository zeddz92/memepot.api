<?php

namespace App\Http\Controllers\Post;

use App\Comment;
use App\Http\Controllers\ApiController;
use App\Media;
use App\Post;
use App\Traits\Image;
use App\Traits\PushNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PostCommentController extends ApiController
{

    use Image;
    use PushNotification;
    /**
     * Display a listing of the resource.
     *
     * @param Post $post
     * @return void
     */
    public function index(Post $post)
    {
        $comments = $post->comments()
            ->with("firstReply", "medias", "userFeedback")
            ->where("parent_id", null)
            ->orderByDesc("created_at")
            ->withCount("comments as replies")
            ->get();
        return $this->showAll($comments);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Post $post
     * @return void
     */
    public function store(Request $request, Post $post)
    {
        $rules = [
            'text' => 'required',
            'user_id' => 'required|exists:users,id',
        ];

        $comment = new Comment();

        $data = null;

        $comment->fill($request->only([
            'text',
            'user_id',
            'parent_id',
        ]));

        if ($request->hasFile('media') && $request->file('media')->isValid()) {
            $data = getimagesize($request->media);
            if (!$data) {
                return $this->errorResponse("Invalid media supplied", 422);
            }
        }

        $comment->save();

        if ($data) {

            $file_name = Storage::disk('medias')->putFile("", $request->media);

            $media = new Media();
            $media->name = $file_name;
            $media->media_url = $file_name;
            $media->thumbnail_url = $this->createThumbnail($file_name, $request->media, $data[1], $data[0]);
            $media->width = $data[0];
            $media->height = $data[1];
            $media->mime = $data["mime"];
            $media->media_type_id = 1;

            $media->save();
            $comment->medias()->syncWithoutDetaching([$media->id]);
        }

        $post->comments()->syncWithoutDetaching([$comment->id]);

        $this->buildNotification($post, $comment);

        $comment = $comment
            ->where("id", $comment->id)
            ->with(["firstReply", "medias", "userFeedback"])
            ->withCount("comments as replies")
            ->first();

        return $this->showOne($comment);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        $this->showOne($comment);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Post $post
     * @param Comment $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post, Comment $comment)
    {
        if (!$post->comments()->find($comment->id)) {
            return $this->errorResponse('The specified comment is not a comment of this post', 404);
        }

        $data = null;

        $rules = $rules = [
            'text' => 'required|min:2',
        ];

        $this->validate($request, $rules);

        if ($request->hasFile('media') && $request->file('media')->isValid()) {
            $data = getimagesize($request->media);
            if (!$data) {
                return $this->errorResponse("Invalid media supplied", 422);
            }
        }

        $comment->text = $request->text;

//        $commentMedia = $comment->medias()->first();
        //
        //        return $commentMedia;

        if ($data) {
            $commentMedia = $comment->medias()->first();

            Storage::disk('medias')->delete($commentMedia->name);
            Storage::disk('media_thumbnails')->delete($commentMedia->thumbnail_url);

            $commentMedia->delete();

            $file_name = Storage::disk('medias')->putFile("", $request->media);

            $media = new Media();
            $media->name = $file_name;
            $media->media_url = $file_name;
            $media->thumbnail_url = $this->createThumbnail($file_name, $request->media, $data[1], $data[0]);
            $media->width = $data[0];
            $media->height = $data[1];
            $media->mime = $data["mime"];
            $media->media_type_id = 1;

            $media->save();
            $comment->medias()->sync([$media->id]);
        }

        $comment->save();

        $comment = $comment
            ->where("id", $comment->id)
            ->with(["firstReply", "medias", "userFeedback"])
            ->withCount("comments as replies")
            ->first();

        return $this->showOne($comment);
    }

    protected function getCommentSIds($item, $array = [])
    {
        $arr = $array;
        if ($item->comments) {
            foreach ($item->comments as $comment) {
                $arr[] = $comment->id;
                if (count($comment->comments) > 0) {
                    $arr = array_merge($arr, $this->getCommentSIds($comment));
                }
            }
        }

        return $arr;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post $post
     * @param Comment $comment
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Post $post, Comment $comment)
    {
        if (!$post->comments()->find($comment->id)) {
            return $this->errorResponse('The specified comment is not a comment of this post', 404);
        }

        //get the children
        $commentsToDetach = $comment->comments()->with("comments.comments")->get()->transform(function ($item, $key) {
            $arr = [];
            $arr[] = $item->id;

            if (count($item->comments) > 0) {
                $arr = array_merge($arr, $this->getCommentSIds($item));
            }
            return $arr;
        })->collapse();

        //attach the parent
        $commentsToDetach[] = $comment->id;
        $post->comments()->detach($commentsToDetach);

        Comment::destroy($commentsToDetach->toArray());
        $comment->delete();

//
        return $this->showOne($comment);
    }

    private function buildNotification($post, $comment)
    {

        $user = $comment->user;
        $token = $user->device_registration_token;
        $title = $user->name . " commented your post";

        $extra = [
            'type' => 'POST_COMMENT',
            'comment_id' => $comment->id,
            'post_id' => $post->id,
        ];

        $this->sendNotification($title, $comment->text, [$token], $extra);
    }
}
