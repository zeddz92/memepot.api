<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\ApiController;
use App\Media;
use App\Post;
use App\Traits\MediaHandler;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PostController extends ApiController
{

    use MediaHandler;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Post $post)
    {
        $category = "";
        $search = "";
        $user_id = 0;
        if (request()->has('category')) {
            $category = request()->category;
        }

        if (request()->has('search')) {
            $search = request()->search;
        }

        if (request()->hasHeader("user_id")) {
            $user_id = request()->header("user_id");

        }

        $posts = $post->with(['medias', 'tags', 'user', 'userFeedback'])->withCount(['comments as comments',
            'feedback as likes' => function ($query) {
                $query->where('feedback', 1);
            },
            'feedback as dislikes' => function ($query) {
                $query->where('feedback', 2);
            },
        ]);

        $launchDay = "2018-12-01";
        switch ($category) {
            case "HOT":
                $posts = $posts->orderByRaw("LOG10(ABS(likes - dislikes) + 1) * SIGN(likes - dislikes) + (UNIX_TIMESTAMP(posts.created_at) / 300000) DESC");
                break;
            case "TRENDING":
                $posts = $posts->orderByRaw("LOG10(ABS(likes - dislikes) + 1) + (SIGN(likes - dislikes) * (UNIX_TIMESTAMP(posts.created_at) - UNIX_TIMESTAMP('{$launchDay}'))/45000 )");
                break;
            case "FRESH":
                $posts = $posts->orderBy("posts.created_at", SORT_DESC);
                break;
            default:
                $posts = $posts->orderBy("posts.created_at", SORT_DESC);
        }

        $posts = $posts->whereHas("tags", function ($query) use ($search) {
            $query->where("name", 'like', '%' . $search . '%');
        })->orWhere("title", 'like', '%' . $search . '%');

        $posts = $posts->get()
            ->unique('id')
            ->values();

        return $this->showAll($posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return "klk";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rules = [
            'title' => 'required',
            'media' => 'required',
            'user_id' => 'required|exists:users,id',
        ];

        $mimeType = $request->media->getMimeType();

        $this->validate($request, $rules);

        if (!$request->file('media')->isValid() || !$this->isValidType($mimeType)) {
            return $this->errorResponse("Invalid media supplied", 422);
        }

        $post = new Post();

        $post->title = $request->title;
        $post->user_id = $request->user_id;
        $post->status = 0;

        $post->save();

        if ($request->has("tags")) {

            $tags = explode(",", $request->tags);
            $post->tags()->syncWithoutDetaching($tags);
        }

        $file = $request->media;

        $metadata = $this->getMediaMetadata($file);

        $file_name = Storage::disk('medias')->putFile("", $file);

        $media = new Media();
        $media->name = $file_name;
        $media->media_url = $file_name;
        $media->thumbnail_url = $this->generateThumbnail($mimeType, $file, $metadata);
        $media->mime = $mimeType;
        $media->width = $metadata->get("width");
        $media->height = $metadata->get("height");
        $media->duration = $metadata->get("duration");
        $media->media_type_id = $this->getMediaTypeId($mimeType);

        $media->save();

        $post->medias()->syncWithoutDetaching([$media->id]);

//        $media = $ffmpeg->open($file)->getStreams()->first()->get("duration");

        return $this->showOne($post);

//        return response()->json(["title" => $data->title]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        // $post = $post
        // // ->with(['medias', 'tags', 'user', 'userFeedback'])
        // ->withCount(['comments as comments',
        //     'feedback as likes' => function ($query) {
        //         $query->where('feedback', 1);
        //     },
        //     'feedback as dislikes' => function ($query) {
        //         $query->where('feedback', 2);
        //     },
        // ])->get()->unique('id')->values();

        $post = $post->with('medias', 'tags', 'user')
            ->where('id', $post->id)
            ->withCount(['comments as comments',
                'feedback as likes' => function ($query) {
                    $query->where('feedback', 1);
                },
                'feedback as dislikes' => function ($query) {
                    $query->where('feedback', 2);
                },
            ])
            ->take(1)
            ->get()
            ->first();

        return $this->showOne($post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        //
        $post->title = $request->title;
        $post->save();
        return $this->showOne($post);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        if (!request()->hasHeader("user_id")) {
            return $this->errorResponse('Request failed', 404);
        }

        
        if ( (int) request()->header("user_id") !== $post->user_id) {
            return $this->errorResponse('This post does not belong to this user', 404);
        }

        $post->delete();
        return $this->showOne($post);
    }

    /**
     * Filters
     * @deprecated
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function filter($id)
    {

    }
}
