<?php

namespace App\Http\Controllers\Comment;

use App\Comment;
use App\CommentFeedBack;
use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;

class CommentFeedBackController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Comment $comment
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Comment $comment)
    {
        $this->validate($request, [
            'user_id' => ['required', 'exists:users,id'],
            'action' => ['required', Rule::in(['1', '2', '0'])]// 1 -> like, 2 -> dislike
        ]);

        $input = $request->all();

        $feedback = CommentFeedBack::where('user_id', $input['user_id'])
            ->where('comment_id', $comment->id)->first();

        if (!$feedback) {
            $input["comment_id"] = $comment->id;
             $feedback = CommentFeedBack::create($input);

        } else {
            $feedback->update(['feedback' => $input['action']]);
        }

        $comment = $comment->where("id", $comment->id)
            ->with("firstReply", "medias", "userFeedback")
            ->withCount("comments as replies")
             ->first();


        return $this->showOne($comment);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment $comment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comment $comment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        //
    }
}
