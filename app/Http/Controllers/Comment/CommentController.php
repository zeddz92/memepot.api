<?php

namespace App\Http\Controllers\Comment;

use App\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CommentFeedBack;
use Illuminate\Validation\Rule;
use App\Http\Controllers\ApiController;

class CommentController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    public function replies(Comment $comment)
    {
        return $this->showAll($comment->comments());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        $rules = [
//            'text' => 'required',
//            'post_id' => 'required',
//            'user_id' => 'required|exists:users,id'
//        ];
//
//        $comment = new Comment();
//
//        $comment->fill($request->only([
//            'text',
//            'user_id'
//        ]));
//
//        $comment->save();
//
//        $post->comments()->syncWithoutDetaching([$comment->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        return $this->showOne($comment);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        Comment::where('id', $id)->update($request->all());
        return $this->response(Comment::where('id', $id)->get());
        
    }

    public function comment(Request $request, $id) // accepts if  $id == root : $id = null
    {
        $id = $id == 'root'? null: $id;
        $this->validate($request, [

            'id' => ['exists:comments,id'],
            'user_id' => ['required', 'exists:users,id'],
            'text' => ['required'],
        ]);

        $input = $request->all();
        
        Comment::create([
            'user_id' => $input['user_id'],
            'text' => $input['text'], 
            'parent_id' => $id
        ]);

        return response()->json(['comment'=>Comment::where('id', \DB::getPdo()->lastInsertId())->first()], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        if (Comment::where('id', $id)->delete()) {
            return response()->json(['deleted'=> true], 200);
        } else {
            return response()->json(['deleted'=> false], 400);
        }
       
        
    }

     /**
     * Update the specified resource in storage.
     *
     * @uri    comments/{idComment}/feedbacks?user_id=1&action=1
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function feedback(Request $request, $id)
    {
        $request['id'] = $id;

        // print_r($request->all());die();
        $this->validate($request, [

            'id' => ['required', 'exists:comments,id'],
            'user_id' => ['required', 'exists:users,id'],
            'action' => ['required', Rule::in(['1', '2', '0'])]// 1 -> like, 2 -> dislike
        ]);

        $input = $request->all();

        if (!CommentFeedBack::where('user_id', $input['user_id'])->where('comment_id', $id)->exists()) {
            CommentFeedBack::create([
                'feedback'=> $input['action'],
                'user_id'=> $input['user_id'],
                'comment_id'=> $id,
            ]);
           
        } else {
            CommentFeedBack::where('user_id', $input['user_id'])
            ->where('comment_id', $id)
            ->update(['feedback'=> $input['action']]);
        }

        return $this->response(CommentFeedBack::where('user_id', $input['user_id'])->where('comment_id', $id)->get());
    }
}
