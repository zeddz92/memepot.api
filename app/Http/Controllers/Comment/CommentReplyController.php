<?php

namespace App\Http\Controllers\Comment;

use App\Comment;
use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommentReplyController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @param Comment $comment
     * @return \Illuminate\Http\Response
     */
    public function index(Comment $comment)
    {
        $replies = $comment->comments()
            ->with("firstReply.medias", "medias")
            ->withCount("comments as replies_count")
            ->orderByDesc("created_at")
            ->get();
        return $this->showEverything($replies);
    }

}
