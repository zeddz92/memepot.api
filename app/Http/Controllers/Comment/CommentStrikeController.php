<?php

namespace App\Http\Controllers\Comment;

use App\Comment;
use App\Http\Controllers\ApiController;
use App\Strike;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommentStrikeController extends ApiController
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Comment $comment
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request, Comment $comment)
    {

        $rules = [
            'user_id' => 'required|exists:users,id',
            'reason_id' => 'required|exists:reasons,id',
        ];

        $this->validate($request, $rules);

        $strike = new Strike();

        $strike->user_id = $request->user_id;
        $strike->reason_id = $request->reason_id;
        $strike->reference = $comment->text;
        $strike->strike_user_id = $comment->user_id;

        $strike->save();

        $comment->strikes()->syncWithoutDetaching([$strike->id]);

        return $this->showOne($strike);

    }


}
