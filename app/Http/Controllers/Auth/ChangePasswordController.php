<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;

class ChangePasswordController extends ApiController
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('guest');
    }

    public function changePassword(Request $request, User $user) {

        $rules = [
            'current_password' => "required|min:6|current_password:" . $user->password,
            'password' => 'required|confirmed|min:6',
        ];

        $this->validate($request, $rules);


        if(strcmp($request->get('current_password'), $request->get('password')) == 0){

            return $this->errorResponse("New Password cannot be same as your current password. Please choose a different password.", 422);
        }

        $user->password = bcrypt($request->get('password'));
        $user->save();

        return $this->showMessage("Password Changed successfully", 0);





    }
}
