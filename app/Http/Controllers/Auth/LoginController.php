<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends ApiController
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
     */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
//    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('guest')->except('logout');
    }

    public function loginWithSocialMedia(Request $request)
    {

        $rules = [
            'name' => 'required',
            'provider_id' => 'required',
            'provider_token' => 'required',
            'provider' => 'required',
        ];

        $this->validate($request, $rules);

        $user = User::where('provider_id', $request->provider_id);
        if($request->email) {
            $user = User::orWhere('email', $request->email);
        }

        $user = $user
            ->with(["following", "setting"])
            ->withCount(['followers', 'following', 'posts'])
            ->get()->first();

        if (!$user) {
            $user = new User();
            $user->fill($request->only([
                'name',
                'email',
                'profile_picture',
                'provider_token',
                'provider_id',
                'provider'
            ]));

            $user->username = time();

            $user->save();

            return $this->authMessage("Logged successfully", false,
            $this->transformResponse($user));
        }


        return $this->authMessage("Logged successfully", false,
        $this->transformResponse($user));

    }

    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        $id = $this->guard()->user()->id;

        $user = User::find($id);

        if ($request->has('token')) {
            $user->device_registration_token = $request->token;
            $user->save();
        }

        $data = User::where('id', $id)
            ->with(["following", "setting"])
            ->withCount(['followers', 'following', 'posts'])
            ->get()->first();

        return $this->authMessage("Logged successfully", false,
            $this->transformResponse($data));

        // return $this->showMessage(["user"=> $this->guard()->user()]);

        //return $this->showMessage("Logged successfully", 200);
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        return $this->authMessage("These credentials do not match our records.", true, null, 422);
//        return $this->errorResponse("These credentials do not match our records.", 422);
    }

    public function transformResponse($data)
    {

        $result = null;

        $result = [

            'user' => [
                'id' => $data->id,
                'name' => $data->name,
                'username' => $data->username,
                'email' => $data->email,
                'profile_picture_url' => $data->profile_picture_url,
                'short_bio' => $data->short_bio,
                'dob' => $data->dob,
                'isAdmin' => $data->isAdmin(),
            ],

            'settings' => $data->setting,

            'following' => $this->getFollowingAsObject($data->following()->with("user")->get()->pluck("user.id")),

            'summary' => [
                'posts' => $data->posts_count ?: 0,
                'followers' => $data->followers_count ?: 0,
                'following' => $data->following_count ?: 0,
            ],
        ];

        return $result;
    }

    public function getFollowingAsObject($following)
    {
        $result = [];
        foreach ($following as &$follow) {
            $result[$follow] = true;
        }

        return $result;
    }
}
