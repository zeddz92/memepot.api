<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\ApiController;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserFeedController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        $feeds = $user
            ->following()
            ->with(["user.posts.user", "user.posts.userFeedback", "user.posts.medias", "user.posts.tags", "user.posts" => function($query) {
                $query->withCount(['comments as comments', 'feedback as likes' => function ($query) {
                    $query->where('feedback', 1);
                },'feedback as dislikes'  => function ($query) {
                    $query->where('feedback', 2);
                }]);

            }])
            ->get()
            ->pluck("user.posts")
            ->values()
            ->collapse()
            ->sortByDesc("created_at");
        return $this->showAll($feeds);
    }


}
