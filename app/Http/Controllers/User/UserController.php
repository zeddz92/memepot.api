<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\ApiController;
use App\Mail\UserCreated;
use App\Post;
use App\Traits\PushNotification;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Imagick;

ini_set('max_execution_time', 0);

class UserController extends ApiController
{
    use PushNotification;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();

        return $this->showAll($users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'username' => 'required|min:4',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|confirmed',
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $data['password'] = bcrypt($request->password);
        $data['verified'] = User::UNVERIFIED_USER;
        $data['verification_token'] = User::generateVerificationCode();
        $data['admin'] = User::REGULAR_USER;
        $data['status'] = 1;

        $user = User::create($data);

        return $this->showOne($user, 201);
    }

    public function createThumbnail($name, $img, $height, $width)
    {
        $imagick = new Imagick(realpath($img));
        $imagick->setImageFormat('jpeg');
        $imagick->setImageCompression(Imagick::COMPRESSION_JPEG);
        $imagick->setImageCompressionQuality(10);
        $imagick->thumbnailImage($width, $height, false, false);
        $filename_no_ext = explode('.', $name)[0];

        if (file_put_contents('../public/thumb/' . $filename_no_ext . '_thumb' . '.jpg', $imagick) === false) {
            throw new Exception("Could not put contents.");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return void
     */
    public function show(User $user)
    {

        $data = User::where('id', $user->id)
            ->withCount(['followers', 'following', 'posts'])
            ->get()->first();
        return $this->showOne($data);
//        User::created(function ($user) {
        //            retry(5, function () use ($user) {
        //                Mail::to($user)->send(new UserCreated($user));
        //            }, 100);
        //        });
        //        return  Mail::to($user)->send(new UserCreated($user));;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        // $comment = Comment::find("5188");
        // $post = $comment->post->first();

        // // print_r($user);
        // return $this->showOne($comment->parent);

        // // $token = $post->user->with('user');

        // // $this->sendNotification("title", "body", null);

        // // echo $user;

        // return $this->showOne($user);
        // $rules = [
        //     'name' => 'min:3',
        //     'username' => 'unique:users,username,' . $user->id,
        //     'dob' => 'date',
        //     'shortBio' => 'min:3',
        // ];

        // $this->validate($request, $rules);

        if ($request->has('name')) {
            $user->name = $request->name;
        }
        if ($request->has('username')) {
            $rules = [
                'username' => 'unique:users,username,' . $user->id,
            ];

            $this->validate($request, $rules);

            $user->username = $request->username;
        }

        if ($request->has('dob')) {
            $user->dob = $request->dob;
        }

        if ($request->has('short_bio')) {
            $user->short_bio = $request->short_bio;
        }

        if ($request->has('token')) {
            $user->device_registration_token = $request->token;
        }

        if ($request->has('profile_picture') && $request->profile_picture !== $user->profile_picture_url) {

            $file_data = $request->profile_picture;
            $file_name = $user->id . time() . ".png";

            @list($type, $file_data) = explode(';', $file_data);
            @list(, $file_data) = explode(',', $file_data);

            $image = base64_decode($file_data);

            Storage::disk('profile_pictures')->delete($user->profile_picture);

            Storage::disk('profile_pictures')->put($file_name, $image);

            $user->profile_picture = $file_name;
        }

        if (!$user->isDirty()) {
            return $this->errorResponse('You need to specify a different value to update', 422);
        }

        $user->save();

        return $this->showOne($user);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(User $user)
    {
        $user->delete();

        return $this->showOne($user);
    }

    public function updateSetting(Request $request, User $user)
    {
        $userSettings = $user->setting;

        $rules = [
            'setting' => 'required',
            'value' => 'required',
        ];

        $this->validate($request, $rules);

        $setting = $request->setting;
        $value = (int) $request->value;

        if (!isset($userSettings->{$setting})) {
            return $this->errorResponse("Setting not found", 422);
        }

        $userSettings->{$setting} = $value;
        $userSettings->save();
        return $this->showOne($userSettings);

    }

    public function verify($token)
    {
        $user = User::where('verification_token', $token)->firstOrFail();

        $user->verified = User::VERIFIED_USER;
        $user->verification_token = null;

        $user->save();

        return $this->showMessage('The account has been verified successfully');
    }

    public function updateToken(User $user)
    {
        $rules = [
            'token' => 'required',
        ];

        $this->validate($request, $rules);
        $user->device_registration_token = $request->token;
        $user->save();

        return $this->showOne($user);
    }

    public function post(User $user, User $poster)
    {
        $post = post::where([
            ["user_id", $user->id],
            ["poster_id", $poster->id],
        ])->first();

        if ($post !== null) {
            $post = new post;
            $post->user_id = $user->id;
            $post->poster_id = $poster->id;
        }

        $post->status = 1;

        $post->save();
        return $this->showOne($post);

    }

    public function unpost(User $user, User $poster)
    {

        $post = post::where([
            ["user_id", $user->id],
            ["poster_id", $poster->id],
        ])->first();

        if ($post === null) {

        }

        $post->status = 0;

        $post->save();
        return $this->showOne($post);
    }

    public function resend(User $user)
    {

        if ($user->isVerified()) {
            return $this->errorResponse('This user is already verified', 409);
        }

        User::created(function ($user) {
            retry(5, function () use ($user) {
                Mail::to($user)->send(new UserCreated($user));
            }, 100);
        });

        return $this->showMessage('The verification email has been sent');
    }

    public function summary($id)
    {
        $data = User::where('id', $id)
            ->withCount(['followers', 'following', 'posts'])
            ->get()->first();
        return $this->showOne($data);
    }
}
