<?php

namespace App\Http\Controllers\User;

use App\Follow;
use App\Http\Controllers\ApiController;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserFollowingController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        $following = $user
            ->following()
            ->with("user")
            ->get()
            ->pluck('user')
            ->values();
        return $this->showAll($following);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
//        $follow = Follow::where([
//            ["user_id", $user->id],
//            ["follower_id", $follower->id]
//        ])->first();
//
//        if ($follow !== null) {
//            $follow = new Follow;
//            $follow->user_id = $user->id;
//            $follow->follower_id = $follower->id;
//        }

        $rules = [
            'user_id' => 'required|exists:users,id'
        ];

        $this->validate($request, $rules);

        if ($user->id == $request->user_id) {
            return $this->errorResponse("User can't follow himself", 422);
        }


        $wasFollowing = Follow::withTrashed()->where([
            ["user_id", $request->user_id],
            ["follower_id", $user->id]
        ])->first();

        if($wasFollowing) {
            $wasFollowing->restore();
            return $this->showOne($wasFollowing);

        }

        $follow = new Follow;
        $follow->user_id = $request->user_id;
        $follow->follower_id = $user->id;

        $follow->save();
        return $this->showOne($follow);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param  \App\User $user
     * @return void
     */
    public function destroy(Request $request, User $user)
    {
        $rules = [
            'user_id' => 'required|exists:users,id'
        ];

        $this->validate($request, $rules);

        $follow = Follow::where([
            ["user_id", $request->user_id],
            ["follower_id", $user->id]
        ])->firstOrFail();

        $follow->delete();

        return $this->showOne($follow);


    }
}
