<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\ApiController;
use App\User;
use Illuminate\Http\Request;

class UserPostController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        $category = "OWN";
        if (request()->has('category')) {
            $category = request()->category;
        }

        $posts = null;

        switch ($category) {
            case "OWN":
                $posts = $this->getUsersPosts($user);
                break;
            case "COMMENTS":
                $posts = $this->getUsersComments($user);
                break;

            case "FAVORITES":
                $posts = $this->getUsersFavorites($user);
                break;

            default:
                $posts = $user->posts()->with(['medias', 'tags', 'user']);
        }

        $posts = $posts;

        return $this->showAll($posts);

    }

    private function getUsersPosts($user)
    {
        return $user->posts()->with(['medias', 'tags', 'user'])->withCount(['comments as comments',
            'feedback as likes' => function ($query) {
                $query->where('feedback', 1);
            },
            'feedback as dislikes' => function ($query) {
                $query->where('feedback', 2);
            },
        ])->orderByDesc('created_at')
            ->get()
            ->values();
    }

    private function getUsersComments($user)
    {
//        return $posts = $user->comments()->with(['post']);
        return $posts = $user->comments()->with(['post.medias', 'post.tags', 'post.user', "post" => function ($query) {
            $query->withCount(['comments as comments', 'feedback as likes' => function ($query) {
                $query->where('feedback', 1);
            }, 'feedback as dislikes' => function ($query) {
                $query->where('feedback', 2);
            }]);

        }])->orderByDesc('created_at')
            ->get()
            ->pluck("post")
            ->collapse()
            ->unique("id");
    }

    private function getUsersFavorites($user)
    {
        return $user->favorites()->with(['post.medias', 'post.tags', 'post.user', "post" => function ($query) {
            $query->withCount(['comments as comments', 'feedback as likes' => function ($query) {
                $query->where('feedback', 1);
            }, 'feedback as dislikes' => function ($query) {
                $query->where('feedback', 2);
            }]);

        }])
            ->orderByDesc('created_at')
            ->get()
            ->pluck("post")
            ->unique("id");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
