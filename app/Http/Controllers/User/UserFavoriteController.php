<?php

namespace App\Http\Controllers\User;

use App\Favorite;
use App\Http\Controllers\ApiController;
use App\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class UserFavoriteController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }ap

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
        $rules = [
            'post_id' => 'required|exists:posts,id',
        ];

        $this->validate($request, $rules);

        $favorite = Favorite::withTrashed()->where('post_id', $request->post_id)
        ->where('user_id', $user->id)->first();
        if(!$favorite) {
            try {
                $favorite = new Favorite();
                $favorite->user_id = $user->id;
                $favorite->post_id = $request->post_id;
                $favorite->save();
                return $this->showOne($favorite);
    
            } catch (QueryException $e) {
                return $this->errorResponse("This user already have this post in favorite", 402);
            } 
        } else  {
            $favorite->restore();
            return $this->showOne($favorite);
        }

        // $user->favorites()->syncWithoutDetaching([$request->post_id]);
        // $alreadyExists
        // if() {

        // }

       

        // $post = Post::find($request->post_id);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        // $rules = [
        //     'post_id' => 'required|exists:posts,id',
        // ];

        // $this->validate($request, $rules);

        if (!request()->has('post_id')) {
            return $this->errorResponse("You need to specify a post id", 402);
        }

        $favorite = Favorite::where('post_id', request()->post_id)
            ->where('user_id', $user->id)->first();
        if (!$favorite) {

            return $this->errorResponse("Record does not exist", 402);
        }
        $favorite->delete();
        return $this->showOne($favorite);

    }
}
