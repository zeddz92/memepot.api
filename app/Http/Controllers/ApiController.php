<?php

namespace App\Http\Controllers;

use App\Traits\ApiResponser;
use App\Traits\Transformers;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    use ApiResponser;
    use Transformers;
}
