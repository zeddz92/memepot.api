<?php

namespace App;

use App\Scopes\CommentScope;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Pagination\LengthAwarePaginator;


/**
 * @property mixed text
 */
class Comment extends Model
{

    use SoftDeletes;

    protected static function boot() {

        parent::boot();
        static::addGlobalScope(new CommentScope());
    }


    protected $fillable = [
        'text', 'user_id', 'parent_id'
    ];

    protected $hidden = [
        'pivot',
    ];

    protected $appends = ['time', 'user', 'likes', 'dislikes'];

    public function medias()
    {
        return $this->belongsToMany(Media::class);
    }

    public function strikes()
    {
        return $this->belongsToMany(Strike::class);
    }

    public function post()
    {
        return $this->belongsToMany(Post::class)->latest();
    }



    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class, 'parent_id');
    }

    public function parent()
    {
        return $this->belongsTo(Comment::class, 'parent_id');
    }

    public function feedback()
    {
        return $this->hasMany(CommentFeedBack::class);
    }

    public function getUserAttribute()
    {
        return $this->user()->get()[0];
    }

    public function getLikesAttribute()
    {
        return $this->feedback()->where("feedback", 1)->get()->count();
    }

    public function getDislikesAttribute()
    {
        return $this->feedback()->where("feedback", 2)->get()->count();
    }

    public function userFeedback()
    {
        $userId = request()->header("user_id");
        return $this->hasOne(CommentFeedBack::class)->select(["comment_id", "feedback as action"])->where("user_id", request()->header("user_id"));
    }

//    public function getCommentsAttribute()
//    {
//        return [];
//    }

    public function getTimeAttribute() {
        return Carbon::parse($this->created_at)->diffForHumans();
    }

    public function firstReply()
    {
        return $this->hasOne(Comment::class, 'parent_id')->orderByDesc('created_at')->withCount("comments as replies_count"); // Order it by anything you want
    }

    protected function paginate($collection)
    {

        $page = 1;

        $perPage = 2;

        $results = $collection->slice(($page - 1) * $perPage, $perPage)->values();

        $paginated = new LengthAwarePaginator($results, $collection->count(), $perPage, $page, [
            'path' => LengthAwarePaginator::resolveCurrentPath()
        ]);

        $paginated->appends(request()->all());

        return $paginated;
    }
}
