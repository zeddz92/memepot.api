<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * @property mixed id
 */
class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    const VERIFIED_USER = '1';
    const UNVERIFIED_USER = '0';

    const ADMIN_USER = 'true';
    const REGULAR_USER = 'false';

    protected $table = 'users';
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'username',
        'profile_picture',
        'dob',
        'shortBio',
        'email',
        'password',
        'verified',
        'verification_token',
        'admin',
        'country',
        'profile_picture',
        'provider_token',
        'provider_id',
        'provider'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'verification_token',
        'updated_at',
        'deleted_at',

    ];

    protected $appends = ['profile_picture_url'];

    public function isVerified() {
        return $this->verified == User::VERIFIED_USER;
    }

    public function isAdmin() {
        return $this->admin == User::ADMIN_USER;
    }

    public static function generateVerificationCode() {
        return str_random(40);
    }

    public function posts() {
        return $this->hasMany(Post::class);
    }

    public function favorites() {
        return $this->hasMany(Favorite::class);
    }


    public function comments() {
        return $this->hasMany(Comment::class);
    }

    public function setting() {
        return $this->hasOne(Setting::class, "user_id");
    }

    public function followers() {
        return $this->hasMany(Follow::class, "user_id");
    }

    public function following() {
        return $this->hasMany(Follow::class, 'follower_id');
    }

//    public function getProfilePictureAttribute($profilePicture) {
//        return url("assets/profile_pictures/{$profilePicture}");
//    }

    public function getProfilePictureUrlAttribute() {
        return $this->profile_picture?  url("assets/profile_pictures/{$this->profile_picture}") : null;
    }
}
