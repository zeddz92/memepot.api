<?php

namespace App\Providers;

use App\Comment;
use App\CommentFeedBack;
use App\Follow;
use App\Mail\UserCreated;
use App\Media;
use App\Post;
use App\PostFeedBack;
use App\Setting;
use App\Traits\PushNotification;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    use PushNotification;
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        Post::created(function ($post) {

            $user = $post->user;

            $tokens = $user->followers()->with("follower")
                ->get()
                ->pluck('follower.device_registration_token')
                ->filter(function ($value, $key) {
                    return $value !== null;
                })
                ->unique()
                ->values();

            $extra = [
                'type' => 'NEW_POST',
                'post' => json_encode($post),
                'user_id' => $post->user_id,
                'post_id' => $post->id,
            ];

            $body = $user->name . " posted something new!";

            $this->sendNotification("", $body, $tokens, $extra);

        });

        Comment::created(function ($comment) {

            // $post = $comment->post->first();

            // $extra = [
            //     'type' => 'POST_COMMENT',
            //     'comment_id' => $comment->id,
            //     'post_id' => $post->id,
            // ];

            // //If  replying to a previous comment
            // if($comment->parent_id)  {
            //     $user = $comment->parent->user;
            //     $token = $user->device_registration_token;
            //     $body = $user->name . " replied to your comment";
            //     $this->sendNotification("", $body, [$token], $extra);
            // }

            // $user = $post->user;
            // $token = $user->device_registration_token;
            // $body = $user->name . " commented your post";
            // $this->sendNotification("", $body, [$token], $extra);

            if ($comment->parent_id) {

                $post = $comment->parent->post->first();

                $extra = [
                    'type' => 'COMMENT_REPLY',
                    'comment_id' => $comment->id,
                    'post_id' => $post->id,
                ];
                $user = $comment->parent->user;
                $token = $user->device_registration_token;
                $title = $user->name . " replied to your comment";
                $this->sendNotification($title, $comment->text, [$token], $extra);
            }

        });

        CommentFeedBack::saved(function ($commentFeedback) {

            $user = $commentFeedback->user;
            if ($commentFeedback->feedback === 1
                // && $user->id !== $commentFeedback->user_id
            ) {

                $token = $user->device_registration_token;

                $extra = [
                    'type' => 'COMMENT_LIKE',
                    'user_id' => $commentFeedback->user_id,
                    'comment_id' => $commentFeedback->comment_id,
                ];
                $body = $user->name . " liked your comment!";

                $this->sendNotification("", $body, [$token], $extra);

            }
        });

        PostFeedBack::saved(function ($postFeedback) {

            $user = $postFeedback->user;
            if ($postFeedback->feedback === 1
                && $user->id !== $postFeedback->user_id
            ) {

                $token = $user->device_registration_token;

                $extra = [
                    'type' => 'POST_LIKE',
                    'user_id' => $postFeedback->user_id,
                    'post_id' => $postFeedback->post_id,
                ];
                $body = $user->name . " liked your post!";

                $this->sendNotification("", $body, [$token], $extra);

            }
        });

        Follow::created(function ($follow) {

            $follower = $follow->follower;
            $user = $follow->user;
            $token = $user->device_registration_token;

            $body = $follower->name . " started following you!";
            $extra = [
                'type' => 'FOLLOW',
                'picture' => $follower->profile_picture_url,
                'follower_id' => $follower->id,
            ];

            $this->sendNotification("", $body, [$token], $extra);
        });

        Media::deleted(function ($media) {
            Storage::disk('medias')->delete($media->media_url);
        });

        User::created(function ($user) {

            $setting = new Setting;
            $setting->user_id = $user->id;
            $setting->save();

            retry(5, function () use ($user) {
                if ($user->email) {
                    Mail::to($user)->send(new UserCreated($user));
                }
            }, 100);
        });

        Validator::extend('current_password', function ($attribute, $value, $parameters, $validator) {
            $userCurrentPassword = $parameters[0];
            return Hash::check($value, $userCurrentPassword);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
