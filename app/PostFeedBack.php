<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class PostFeedBack extends Model
{
    use Notifiable, SoftDeletes;

    protected $fillable = [
        'user_id',
        'post_id',
        'feedback',
    ];


    protected $table = 'post_feedback';

    public function post() {
        return $this->belongsTo(Post::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }
}
