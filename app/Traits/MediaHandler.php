<?php

namespace App\Traits;

use FFMpeg\Coordinate\TimeCode;
use FFMpeg\Media\Frame;
use Illuminate\Support\Facades\Storage;
use FFMpeg\FFMpeg;
use FFMpeg\FFProbe;
use Imagick;


trait MediaHandler
{
    use Image;
    private $logger = null;
    private $validTypes = ["image", "video", "audio"];

    private $configuration = [
        'ffmpeg.binaries' => '/Applications/Mamp/bin/php/php7.2.1/bin/ffmpeg',
        'ffprobe.binaries' => '/Applications/Mamp/bin/php/php7.2.1/bin/ffprobe',
        'timeout' => 3600, // The timeout for the underlying process
        'ffmpeg.threads' => 12,   // The number of threads that FFMpeg should use
    ];

    public function isImage($mimeType)
    {
        $mime = explode("/", $mimeType);
        return $mime[0] === "image";
    }

    public function getMediaTypeId($mimeType)
    {
        $mime = explode("/", $mimeType);

        switch ($mime[0]) {
            case "image":
                return 1;
            case "video":
                return 2;
            case "audio":
                return 3;
            default:
                return 0;
        }

    }

    public function isValidType($mimeType)
    {
        $mime = explode("/", $mimeType);
        return in_array($mime[0], $this->validTypes);
    }

    public function generateThumbnail($mimeType, $file, FFProbe\DataMapping\AbstractData $metadata) {

        $mime = explode("/", $mimeType);
        $filename = time() . ".png";

        if($mime[0] === "video") {
            $ffmpeg = FFMpeg::create($this->configuration, $this->logger, null);
            $ffmpeg->open($file)->frame(TimeCode::fromSeconds(0))->save('thumb/' . $filename);
            return $filename;
        } else if($mime[1] === "image") {
            return $this->createThumbnail($filename, $file, $metadata->get("height"), $metadata->get("width"));
        }


        return null;

    }

    public function getMediaMetadata($file)
    {
        $ffmpeg = FFMpeg::create($this->configuration, $this->logger, null);
        return $ffmpeg->open($file)->getStreams()->first();
    }

}