<?php
namespace App\Traits;

trait PushNotification {

   
    private $URL = 'https://fcm.googleapis.com/fcm/send';
  
    // private $HEADERS = [
    //     'Authorization: key=' . $this->API_ACCESS_KEY,
    //     'Content-Type: application/json'
    // ];
    
    public function sendNotification($title, $body, $tokens, $extra) 
    {
        $headers = [
            'Authorization: key=' . env("API_ACCESS_KEY", ""),
            'Content-Type: application/json'
        ];
        
        $notification = [
            'title' =>$title,
            'body' => $body,
            'sound' => "default",
            'color' => "#203E78" 
        ];
        $extraNotificationData = ["message" => $notification,"moredata" =>'dd'];

        $token = "eXflZAZqVRU:APA91bEaiG4E1oYTgMBhg9DKIfCuA3H8infyh_3Lg9ktZRrFJ_DnPnSjMmE5yxBQAgppLyAWOVbbN_iSGfKrbTVMLMSLd2wDxrtYFTmyELXwe3dn9tWpM3HPP2Wwea2Q5Am_NojSb-51f9v6yq-ivt015JCbsSk-nA";
        $tokens = [
            "eXflZAZqVRU:APA91bEaiG4E1oYTgMBhg9DKIfCuA3H8infyh_3Lg9ktZRrFJ_DnPnSjMmE5yxBQAgppLyAWOVbbN_iSGfKrbTVMLMSLd2wDxrtYFTmyELXwe3dn9tWpM3HPP2Wwea2Q5Am_NojSb-51f9v6yq-ivt015JCbsSk-nA",
            "cJvZ1UlGoEE:APA91bHw-rVr-0esNg77h6TTBOT_Dysq253-ng3c380GhcPaVDovFQ-WWTJoEzhLrukXdUABdlO_f6raXNMeHfZxj6aYTjXMl1vflgs5YkkmlSZ9cN3PMHhdH8xhQhD1dO_DgWIQjW9mqkIQ4OCNE75t7no790KVsw"
        ];

        $fcmNotification = [
            'registration_ids' => $tokens, //multple token array
            // 'to'        => $token, //single token
            'notification' => $notification,
            'data' => $extra
        ];

  
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->URL);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        // print_r($result);
        curl_close($ch);
    }

}