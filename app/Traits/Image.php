<?php

namespace App\Traits;
use Illuminate\Support\Facades\Storage;

use Imagick;


trait Image {

    public function createThumbnail($name, $img, $height, $width)
    {
        $imagick = new Imagick(realpath($img));
        $imagick->setImageFormat('jpeg');
        $imagick->setImageCompression(Imagick::COMPRESSION_JPEG);
        $imagick->setImageCompressionQuality(10);
        $imagick->thumbnailImage($width, $height, false, false);
        $filename_no_ext = explode('.', $name)[0];

        $file_name = $filename_no_ext  . '.jpg';
        Storage::disk('media_thumbnails')->put($file_name, $imagick);

        return $file_name;

    }

}