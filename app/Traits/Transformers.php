<?php

namespace App\Traits;
trait Transformers {

    protected function authTransformer($data)
    {

        $result = null;

        $result = [

            'user' => [
                'id' => $data->id,
                'name' => $data->name,
                'username' => $data->username,
                'email' => $data->email,
                'profile_picture_url' => $data->profile_picture_url,
                'short_bio' => $data->short_bio,
                'dob' => $data->dob,
                'isAdmin' => $data->isAdmin()
            ],

            'settings' => $data->setting,

            'following' => $this->getFollowingAsObject($data->following()->with("user")->get()->pluck("user.id")),

            'summary' => [
                'posts' => $data->posts_count,
                'followers' => $data->followers_count,
                'following' => $data->following_count
            ]
        ];

        return $result;
    }

    private function getFollowingAsObject($following) {
        $result = [];
        foreach ($following as &$follow) {
            $result[$follow] = true;
        }

        return $result;
    }
}