<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Cache;

trait ApiResponser {
    private function successResponse($data, $code) {
        return response()->json($data, $code);
    }

    protected function errorResponse($message, $code) {
        return response()->json(['error' => $message, 'code' => $code], $code);
    }

    protected function showEverything(Collection $collection, $code = 200) {

        $collection = $this->filterData($collection);
        $collection = $this->sortData($collection);
//        $collection = $this->cacheResponse($collection);

        return $this->successResponse(["data"=> $collection], $code);
    }

    protected function showAll(Collection $collection, $code = 200) {

        $collection = $this->filterData($collection);
        $collection = $this->sortData($collection);
        $collection = $this->paginate($collection);
//        $collection = $this->cacheResponse($collection);

        return $this->successResponse($collection, $code);
    }

    protected function response(Collection $collection, $code = 200) {// No cache
 
        $collection = $this->filterData($collection);
        $collection = $this->sortData($collection);
        $collection = $this->paginate($collection);

        return $this->successResponse($collection, $code);
    }

    protected function showOne(Model $model, $code = 200) {
        return $this->successResponse(['data' => $model], $code);
    }

    protected function filterData(Collection $collection)
    {
        if(count($collection) === 0) {
            return $collection;
        }
        $model = $collection->first();
        foreach (request()->query() as $query => $value) {
            if($model->{$query}) {
                $collection = $collection->where($query, $value);
            }
        }
        return $collection;
    }

    protected function sortData(Collection $collection)
    {
        if (request()->has('sort_by')) {
            $attribute = request()->sort_by;
            $collection = $collection->sortBy()->${$attribute};
        }
        return $collection;
    }


    protected function paginate(Collection $collection)
    {
        $rules = [
            'per_page' => 'integer|min:2|max:50',
        ];

        Validator::validate(request()->all(), $rules);

        $page = LengthAwarePaginator::resolveCurrentPage();

        $perPage = 15;
        if (request()->has('per_page')) {
            $perPage = (int)request()->per_page;
        }

        $results = $collection->slice(($page - 1) * $perPage, $perPage)->values();

        $paginated = new LengthAwarePaginator($results, $collection->count(), $perPage, $page, [
            'path' => LengthAwarePaginator::resolveCurrentPath()
        ]);

        $paginated->appends(request()->all());

        return $paginated;
    }

    protected function cacheResponse($data)
    {
        $url = request()->url();
        $queryParams = request()->query();
        ksort($queryParams);
        $queryString = http_build_query($queryParams);
        $fullUrl = "{$url}?{$queryString}";
        return Cache::remember($fullUrl, 30 / 60, function () use ($data) {
            return $data;
        });
    }

    protected function showMessage($text, $error = 0, $code = 200) {
        return response()->json(['message' => $text, 'code' => $code, "error" => $error], $code);

    }

    protected function authMessage($message, $error, $data, $code = 200)
    {
        return $this->successResponse([
            "status" => [
                "type" => $error ? "error" : "success",
                "message" => $message,
                "code" => $code,
                "error" => $error
            ],
            'data' => $data], $code);
    }


}
