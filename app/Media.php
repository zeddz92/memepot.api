<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{

    protected $table = 'medias';

    protected $fillable = [
        'url', 'type'
    ];

    protected $hidden = [
        'pivot',
    ];


    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function comment()
    {
        return $this->belongsTo(Comment::class);
    }

    public function getMediaUrlAttribute($mediaUrl) {
        return url("img/{$mediaUrl}");
    }

    public function getThumbnailUrlAttribute($thumbnail) {
        return url("thumb/{$thumbnail}");
    }



}
