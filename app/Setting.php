<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $hidden = [
        'user_id',
        'created_at',
        'updated_at',
        'id'
    ];
}
