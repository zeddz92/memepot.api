<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \App\Favorite;
use \App\Media;

/**
 * @property int user_id
 * @property string title
 * @property int status
 */
class Post extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'title', 'user_id',
    ];

    protected $hidden = [
        'pivot',
    ];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $appends = ['time', 'favorite'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function medias()
    {
        return $this->belongsToMany(Media::class);
    }

    public function feedback()
    {
        return $this->hasMany(PostFeedBack::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function userFeedback()
    {
        $userId = request()->header("user_id");
        return $this->hasOne(PostFeedBack::class)->select(["post_id", "feedback as action"])->where("user_id", request()->header("user_id"));
    }

    public function userFavorite()
    {
        $userId = request()->header("user_id");
        return $this->hasOne(Favorite::class)->select(["post_id"])->where("user_id", request()->header("user_id"));
    }

    public function getFavoriteAttribute()
    {
        if (request()->hasHeader("user_id")) {
            if (Favorite::
                where('user_id', request()->hasHeader("user_id"))
                ->where('post_id', $this->id)
                ->first()) {
                return true;
            }
        }
        return false;
    }

    public function comments()
    {
        return $this->belongsToMany(Comment::class);
    }

    public function getTimeAttribute()
    {
        return Carbon::parse($this->created_at)->diffForHumans();
    }

    public function strikes()
    {
        return $this->belongsToMany(Strike::class);
    }

}
