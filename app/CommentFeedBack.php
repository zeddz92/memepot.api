<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentFeedBack extends Model
{

    protected $fillable = [
        'user_id',
        'comment_id',
        'feedback',
    ];

    protected $table = 'comment_feedback';

    public function comment() {
        return $this->belongsTo(Comment::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }
}
